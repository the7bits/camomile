Feature: Index page
    In order have a full featured index page
    As a user
    I want to see buttons on it

    Scenario: Open the index page as anonymous user
        Given I visit the django url "/"
        When I look around
        Then I should see the link to "/accounts/login/"
        And I should see the link to "/accounts/request-invite/"

    Scenario: Open the index page as authorized user
        Given I log in as user "admin@testmail.com" with password "123456"
        And I visit the django url "/"
        When I look around
        Then I should see that the element with id "nickname-info" contains exactly "admin"
        And I should see the link to "/dashboard/"

