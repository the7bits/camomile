# coding: utf-8
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    args = ''
    help = 'Initializes Camomile'

    def handle(self, *args, **options):
        from django.contrib.auth import get_user_model

        from django.conf import settings
        customer = get_user_model()
        u_admin = customer.objects.create_user(
                email='admin@testmail.com',
                nickname='admin',
                password='123456',
                )
        u_admin.is_admin = True
        u_admin.is_superuser = True
        u_admin.activation_code = 'ALREADY_ACTIVATED'
        u_admin.save()

        if 'test_mode' in args:
            for i in range(1, 10):
                u = customer.objects.create_user('user%d@testmail.com'%i, 'user%d'%i, '123456')
                u.activation_code = 'ALREADY_ACTIVATED'
                u.save()
