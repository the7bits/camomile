# coding: utf-8
from lettuce import step, world
from lettuce.django import django_url, mail

@step(u'log in as user "(.*)" with password "(.*)"')
def log_in_as_user(step, username, password):
    world.browser.visit(django_url('/accounts/login/'))
    world.browser.fill('username', username)
    world.browser.fill('password', password)
    world.browser.find_by_css('input[type=submit]').click()

@step(u'an email is sent to "([^"]*?)" with subject "([^"]*)"')
def email_sent(step, to, subject):
    message = mail.queue.get(True, timeout=5)
    assert message.subject == subject
    assert to in message.recipients()
