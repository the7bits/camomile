# unicode: utf-8
from __future__ import unicode_literals
from django_rq import job

from projects.models import Project

@job
def prepare_project(project_id):
    project = Project.objects.get(id=project_id)
    project.prepare()

@job
def initialize_project(project_id):
    project = Project.objects.get(id=project_id)
    project.initialize()

@job
def generate_project(project_id):
    project = Project.objects.get(id=project_id)
    project.generate()

@job
def update_project(project_id):
    project = Project.objects.get(id=project_id)
    project.update()

@job
def deploy_project(project_id):
    project = Project.objects.get(id=project_id)
    project.deploy()

