# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _

from projects.models import ProjectTemplate, Project, FileType, HiddenFile
from projects.tasks import generate_project,\
    prepare_project, initialize_project, deploy_project, update_project

class ProjectTemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'remote_repo', 'branch')

class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        'identifier', 'url', 'title', 
        'project_template', 'author', 'pub_date',
        'locked', 'status')
    actions = [
        'prepare_projects', 
        # 'print_config', 
        'init_project',
        'generate_project',
        'update_project',
        'deploy_project',
        ]
    
    def prepare_projects(self, request, queryset):
        for q in queryset:
            prepare_project.delay(project_id=q.id)
    prepare_projects.short_description = _('Prepare projects')

    # def print_config(self, request, queryset):
    #     for q in queryset:
    #         print q.get_config()
    # print_config.short_description = _('Print config')

    def init_project(self, request, queryset):
        for q in queryset:
            initialize_project.delay(project_id=q.id)
    init_project.short_description = _('Init')

    def generate_project(self, request, queryset):
        for q in queryset:
            generate_project.delay(project_id=q.id)
    generate_project.short_description = _('Generate')

    def update_project(self, request, queryset):
        for q in queryset:
            update_project.delay(project_id=q.id)
    update_project.short_description = _('Update')

    def deploy_project(self, request, queryset):
        for q in queryset:
            deploy_project.delay(project_id=q.id)
    deploy_project.short_description = _('Deploy')

class FileTypeAdmin(admin.ModelAdmin):
    list_display = ('extension', 'mode')

class HiddenFileAdmin(admin.ModelAdmin):
    pass

admin.site.register(ProjectTemplate, ProjectTemplateAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(FileType, FileTypeAdmin)
admin.site.register(HiddenFile, HiddenFileAdmin)
