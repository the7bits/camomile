# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ProjectTemplate'
        db.create_table(u'projects_projecttemplate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('description', self.gf('django.db.models.fields.TextField')()),
            ('remote_repo', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('branch', self.gf('django.db.models.fields.CharField')(max_length=80)),
        ))
        db.send_create_signal(u'projects', ['ProjectTemplate'])


    def backwards(self, orm):
        # Deleting model 'ProjectTemplate'
        db.delete_table(u'projects_projecttemplate')


    models = {
        u'projects.projecttemplate': {
            'Meta': {'object_name': 'ProjectTemplate'},
            'branch': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'remote_repo': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['projects']