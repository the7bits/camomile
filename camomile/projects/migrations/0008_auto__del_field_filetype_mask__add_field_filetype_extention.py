# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'FileType.mask'
        db.delete_column(u'projects_filetype', 'mask')

        # Adding field 'FileType.extention'
        db.add_column(u'projects_filetype', 'extention',
                      self.gf('django.db.models.fields.CharField')(default='coffee', max_length=50),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'FileType.mask'
        db.add_column(u'projects_filetype', 'mask',
                      self.gf('django.db.models.fields.CharField')(default='coffee', max_length=80),
                      keep_default=False)

        # Deleting field 'FileType.extention'
        db.delete_column(u'projects_filetype', 'extention')


    models = {
        u'projects.filetype': {
            'Meta': {'object_name': 'FileType'},
            'extention': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mode': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'projects.project': {
            'Meta': {'object_name': 'Project'},
            'author': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'destination_dir': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '80'}),
            'keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'last_generate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 2, 23, 0, 0)'}),
            'locked': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'project_template': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['projects.ProjectTemplate']"}),
            'pub_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2013, 2, 23, 0, 0)'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "u'new'", 'max_length': '20'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'projects.projecttemplate': {
            'Meta': {'object_name': 'ProjectTemplate'},
            'branch': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'remote_repo': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['projects']