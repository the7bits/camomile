# coding: utf-8
from __future__ import unicode_literals
import os
import subprocess
import json
import yaml
import markdown

from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.utils import six
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

PROJECT_STATUS = (
    ('new', _('New')),
    ('initializing', _('Initializing')),
    ('initialized', _('Initialized')),
    ('updating', _('Updating')),
    ('updated', _('Updated')),
    ('generating', _('Generating')),
    ('generated', _('Generated')),
    ('deploying', _('Deploying')),
    ('deployed', _('Deployed')),
)

@python_2_unicode_compatible
class FileType(models.Model):
    extension = models.CharField(_('Extension'), max_length=50, unique=True)
    mode = models.CharField(max_length=255)

    def __str__(self):
        return six.text_type(self.extension)

@python_2_unicode_compatible
class HiddenFile(models.Model):
    filename = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return six.text_type(self.filename)


@python_2_unicode_compatible
class ProjectTemplate(models.Model):
    name = models.CharField(_('Name'), max_length=80)
    description = models.TextField(_('Description'))
    remote_repo = models.CharField(max_length=255)
    branch = models.CharField(max_length=80)

    def __str__(self):
        return six.text_type(self.name)


@python_2_unicode_compatible
class Project(models.Model):
    identifier = models.SlugField(max_length=80, unique=True)
    project_template = models.ForeignKey(ProjectTemplate)

    url = models.URLField()
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    keywords = models.TextField(blank=True)
    author = models.CharField(max_length=255, blank=True)
    email = models.EmailField(blank=True)  

    pub_date = models.DateTimeField(default=timezone.now())
    last_generate = models.DateTimeField(default=timezone.now())
    status = models.CharField(max_length=20, choices=PROJECT_STATUS, default='new')
    locked = models.BooleanField(default=False)

    destination_dir = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return six.text_type(self.title)

    def prepare(self):
        os.chdir(getattr(settings, 'PROJECTS_WORKING_DIR', ''))        
        subprocess.call(
                'git clone -b {0} {1} {2}'.format(
                        self.project_template.branch,
                        self.project_template.remote_repo,
                        self.identifier,
                        ), 
                shell=True)

    def get_project_dir(self):
        return os.path.join(
                getattr(settings, 'PROJECTS_WORKING_DIR', ''),
                self.identifier
                )

    def get_src_dir(self):
        return os.path.join(
                self.get_project_dir(),
                'src'
                )

    def get_config_file_name(self):
        return 'project.json'

    def get_full_file_name(self, filename):
        return os.path.join(
            self.get_project_dir(),
            filename
            )    

    def get_config(self):
        config_body = open(
                os.path.join(
                        self.get_project_dir(),
                        self.get_config_file_name()
                        ),
                'r'
                ).read()
        return json.loads(config_body)

    def run_action(self, name, **kwargs):
        config = self.get_config()
        if 'actions' in config:
            for action in config['actions']:
                if action['name'] == name:
                    self.locked = True
                    self.status = {
                        'init': 'initializing',
                        'generate': 'generating',
                        'update': 'updating',
                        'deploy': 'deploying',
                    }[name]
                    self.save(update_fields=['locked', 'status'])

                    commands = action['commands']
                    os.chdir(self.get_project_dir())
                    for command in commands:
                        subprocess.call(command.format(
                                destination_dir=self.destination_dir,
                                identifier=self.identifier,
                                branch=self.project_template.branch
                                ), shell=True)
                    
                    self.locked = False
                    self.status = {
                        'init': 'initialized',
                        'generate': 'generated',
                        'update': 'updated',
                        'deploy': 'deployed',
                    }[name]
                    self.save(update_fields=['locked', 'status'])
                    return True
        return False

    def initialize(self):
        return self.run_action('init')

    def generate(self):
        return self.run_action('generate')

    def update(self):
        return self.run_action('update')

    def deploy(self):
        return self.run_action('deploy')

    def get_collection_list(self):
        config = self.get_config()
        return config.get('collections', [])

    def get_collection(self, name, **kwargs):
        config = self.get_config()
        if 'collections' in config:
            for collection in config['collections']:
                if collection['name'] == name:
                    collection_dir = os.path.join(
                        self.get_src_dir(),
                        collection['directory']
                        )

                    documents_meta_data = []                    
                    for f in os.listdir(collection_dir):
                        if f.endswith(collection.get('document_extention', '')):                            
                            filename = os.path.join(collection_dir, f)
                            metablock = ''
                            delimiter = 0
                            for line in open(filename, 'r'):
                                line = line.decode('utf-8')
                                if line.strip() == '---': delimiter += 1
                                if 0 < delimiter < 2: metablock += line
                                if delimiter >=2: break                                
                            metadata = yaml.load(metablock)
                            metadata['full_filename'] = filename
                            metadata['filename'] = f
                            documents_meta_data.append(metadata)
                    return collection, documents_meta_data
        return None, None

    def get_document(self, collection_name, file_name, **kwargs):
        config = self.get_config()
        if 'collections' in config:
            for collection in config['collections']:
                if collection['name'] == collection_name:
                    collection_dir = os.path.join(
                        self.get_src_dir(),
                        collection['directory']
                        )
                    for f in os.listdir(collection_dir):
                        if f == file_name:
                            filename = os.path.join(collection_dir, f)
                            metablock = content = ''
                            delimiter = 0
                            for line in open(filename, 'r'):
                                line = line.decode('utf-8')
                                if line.strip() == '---' and delimiter < 2: 
                                    delimiter += 1
                                elif delimiter >= 2:
                                    content += line
                                if 0 < delimiter < 2: metablock += line
                            document = yaml.load(metablock)
                            document['full_filename'] = filename
                            document['filename'] = f
                            document['content'] = content
                            return document
        return None



