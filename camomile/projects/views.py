# coding: utf-8
from __future__ import unicode_literals
import os
import json

from django.utils.translation import ugettext as _
from django.views.generic.edit import CreateView, FormView
from django.views.generic import ListView, DetailView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.contrib import messages

from mail.utils import send_mail
from projects.models import ProjectTemplate, Project, FileType, HiddenFile
from projects.forms import NewFileForm, RenameFileForm, NewFolderForm

HOME_DIR = os.path.expanduser('~')

@login_required
def public_key_view(request):
    key_file = os.path.join(
        HOME_DIR, 
        '.ssh/{0}'.format(getattr(settings, 'PUBLIC_KEY_FILE_NAME', 'id_dsa.pub'))
        )
    public_key = open(key_file, 'r').read()
    return render(request, 'projects/server_public_key.html', {
            'public_key': public_key,
            })

class ProjectTemplateList(ListView):
    model = ProjectTemplate
    context_object_name = 'project_templates'

class ProjectList(ListView):
    model = Project
    context_object_name = 'projects'

class ProjectDetail(ListView):
    model = Project
    template_name = 'projects/project.html'
    context_object_name = 'project'

    def get_queryset(self):
        return Project.objects.get(identifier=self.kwargs['project_identifier'])

@login_required
def file_view(request, project_identifier, path):
    project = Project.objects.get(identifier=project_identifier)
    project_dir = project.get_project_dir()
    filename = project.get_full_file_name(path)
    try:
        if os.path.isdir(filename):
            files = []            
            hidden_files_objs = HiddenFile.objects.all()
            hidden_files = [x.filename for x in hidden_files_objs]

            for fn in sorted(os.listdir(filename)):
                relpath = os.path.relpath(
                        os.path.join(filename, fn),
                        project_dir)
                if relpath not in hidden_files:
                    files.append({
                        'name': fn,
                        'full_name': relpath,
                        'size': os.path.getsize(os.path.join(filename, fn)),
                        'is_dir': os.path.isdir(os.path.join(filename, fn)),
                    })
            
            files = sorted(files, key=lambda x:x['is_dir'], reverse=True)

            pardir = None
            if os.path.abspath(project_dir) != os.path.abspath(filename):
                pardir = os.path.relpath(
                    os.path.abspath(os.path.join(filename, os.pardir)),
                    project_dir
                    )
                files = [{
                    'name': '..',
                    'full_name': pardir,
                    'is_dir': True,
                }] + files

            return render(request, 'projects/directory.html', {
                'project': project,
                'path': path,
                'files': files,
                'pardir': pardir,
                })
        else:
            src = open(filename, 'r').read()
            basename = os.path.basename(filename)
            extension = os.path.splitext(filename)[1]
            try:
                file_type = FileType.objects.get(extension=extension).mode
            except FileType.DoesNotExist:
                file_type = 'text'

            return render(request, 'projects/file.html', {
                'project': project,
                'path': path,
                'dirname': os.path.dirname(path),
                'src': src,
                'mode': file_type,
                })
    except IOError:
        return render(request, 'projects/no_such_file.html', {
            'project': project,
            'path': path,
            })


@login_required
def file_save(request):
    context = {'success':False}

    if request.method == 'POST':
        project_identifier = request.POST['project_identifier']
        project = Project.objects.get(identifier=project_identifier)
        filename = project.get_full_file_name(request.POST['path'])
        src = request.POST['src'].encode('utf-8')
        f = open(filename, 'w')
        f.write(src)
        f.close()
        context = {'success':True}
    
    return HttpResponse(
        json.dumps(context),
        content_type = 'application/json'
    )


class NewFile(FormView):
    form_class = NewFileForm
    template_name = 'projects/new_file.html'

    def get_context_data(self, **kwargs):
        context = super(NewFile, self).get_context_data(**kwargs)  
        project = Project.objects.get(identifier=self.kwargs['project_identifier'])
        context['project'] = project
        context['path'] = self.kwargs['path']
        return context    

    def dispatch(self, *args, **kwargs):
        self.success_url = reverse('project_file', kwargs={
                'project_identifier': kwargs['project_identifier'],
                'path': kwargs['path'],
                })
        return super(NewFile, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        project = Project.objects.get(identifier=self.kwargs['project_identifier'])
        basepath = project.get_full_file_name(self.kwargs['path'])
        filename = os.path.join(basepath, form.cleaned_data['filename'])
        open(filename, 'w').write('')
        return super(NewFile, self).form_valid(form)

@login_required
def delete_file_view(request, project_identifier, path):
    project = Project.objects.get(identifier=project_identifier)
    project_dir = project.get_project_dir()
    target_file = os.path.join(project_dir, path)
    target_dir = os.path.relpath(
                    os.path.dirname(target_file),
                    project_dir
                    )
    if os.path.isdir(target_file):
        import shutil
        shutil.rmtree(target_file)
    else:
        os.remove(target_file)
    return redirect(reverse('project_file', kwargs={
            'project_identifier': project_identifier,
            'path': target_dir,
            }))

class RenameFile(FormView):
    form_class = RenameFileForm
    template_name = 'projects/rename_file.html'

    def get_context_data(self, **kwargs):
        context = super(RenameFile, self).get_context_data(**kwargs)  
        project = Project.objects.get(identifier=self.kwargs['project_identifier'])
        context['project'] = project
        context['path'] = self.kwargs['path']
        return context    

    def get_initial(self):
        path = self.kwargs['path']
        filename = os.path.basename(path)
        return {'filename': filename}

    def dispatch(self, *args, **kwargs):
        self.success_url = reverse('project_file', kwargs={
                'project_identifier': kwargs['project_identifier'],
                'path': os.path.dirname(kwargs['path']),
                })
        return super(RenameFile, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        project = Project.objects.get(identifier=self.kwargs['project_identifier'])
        oldpath = project.get_full_file_name(self.kwargs['path'])
        newpath = os.path.join(os.path.dirname(oldpath), form.cleaned_data['filename'])
        os.rename(oldpath, newpath)
        return super(RenameFile, self).form_valid(form)

class NewFolder(FormView):
    form_class = NewFolderForm
    template_name = 'projects/new_folder.html'

    def get_context_data(self, **kwargs):
        context = super(NewFolder, self).get_context_data(**kwargs)  
        project = Project.objects.get(identifier=self.kwargs['project_identifier'])
        context['project'] = project
        context['path'] = self.kwargs['path']
        return context    

    def dispatch(self, *args, **kwargs):
        self.success_url = reverse('project_file', kwargs={
                'project_identifier': kwargs['project_identifier'],
                'path': kwargs['path'],
                })
        return super(NewFolder, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        project = Project.objects.get(identifier=self.kwargs['project_identifier'])
        basepath = project.get_full_file_name(self.kwargs['path'])
        foldername = os.path.join(basepath, form.cleaned_data['foldername'])
        os.mkdir(foldername)
        return super(NewFolder, self).form_valid(form)

@login_required
def generate_project_view(request, project_identifier):
    from projects.tasks import generate_project
    project = Project.objects.get(identifier=project_identifier)
    generate_project.delay(project_id=project.id)
    messages.info(request, _('Your site added to queue for generation.'))
    return redirect(reverse('project', kwargs={
            'project_identifier': project_identifier,
            }))
