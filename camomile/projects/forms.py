# coding: utf-8
from __future__ import unicode_literals
from django import forms

class NewFileForm(forms.Form):
    filename = forms.CharField(min_length=1, required=True)

class RenameFileForm(forms.Form):
    filename = forms.CharField(min_length=1, required=True)

class NewFolderForm(forms.Form):
    foldername = forms.CharField(min_length=1, required=True)
