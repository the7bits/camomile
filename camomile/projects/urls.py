# coding: utf-8
from django.conf.urls.defaults import patterns, include, url
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from projects.views import ProjectTemplateList, ProjectList, ProjectDetail, NewFile, RenameFile, NewFolder

urlpatterns = patterns('',
    # url(r'^templates/$', login_required(ProjectTemplateList.as_view()), name="project_templates"),
    url(r'^projects/$', login_required(ProjectList.as_view()), name="projects"),
    url(r'^project/(?P<project_identifier>\w+)/generate/$', 'projects.views.generate_project_view', name="generate_project"),
    url(r'^project/(?P<project_identifier>\w+)/$', login_required(ProjectDetail.as_view()), name="project"),
    url(r'^project/(?P<project_identifier>\w+)/new-file-in/(?P<path>.*)$', login_required(NewFile.as_view()), name="new_file"),
    url(r'^project/(?P<project_identifier>\w+)/new-folder-in/(?P<path>.*)$', login_required(NewFolder.as_view()), name="new_folder"),
    url(r'^project/(?P<project_identifier>\w+)/delete-file/(?P<path>.*)$', 'projects.views.delete_file_view', name="delete_file"),
    url(r'^project/(?P<project_identifier>\w+)/rename-file/(?P<path>.*)$', login_required(RenameFile.as_view()), name="rename_file"),
    url(r'^project/(?P<project_identifier>\w+)/files/(?P<path>.*)$', 'projects.views.file_view', name="project_file"),
    url(r'^file-save/$', 'projects.views.file_save', name="file_save"),

    # url(r'^project/(\w+)/(\w+)$', 'projects.views.collection_item_list', name="collection_item_list"),
    # url(r'^public-key/$', 'projects.views.public_key_view', name="server_public_key"),    
)