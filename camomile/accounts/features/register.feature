Feature: User register
    In order to register the new user
    As a user
    I want to register my account

    Scenario: Register by invite
        Given I have an invite for "user20@testmail.com"
        When I visit the register by invite page
        And I fill in the field named "email" with "user20@testmail.com"
        And I fill in the field named "nickname" with "User20"
        And I fill in the field named "password1" with "123456"
        And I fill in the field named "password2" with "123456"
        And I click on the button named "submit"
        Then I should see the element with the css selector ".alert-success"
        And I should see an email is sent to "user20@testmail.com" with subject "Hello"