# coding: utf-8
from salad.steps.everything import *
from lettuce import step
from lettuce.django import django_url
from accounts.models import Invite

@step(u'Given I have an invite for "([^"]*)"')
def given_i_have_an_invite_for_group1(step, email):
    invite = Invite.objects.create(email=email)
    world.invite_code = invite.code

@step(u'When I visit the register by invite page')
def when_i_visit_the_register_by_invite_page(step):
    world.browser.visit(
            django_url('/accounts/register/?invite={0}').format(world.invite_code)
            )
    
