{% extends "mail/base.txt" %}
{% load i18n %}
{% block content %}
{% blocktrans %}
Your just created new account and need to activate it by following the next link:

{{ domain }}/accounts/activate/{{ nickname }}/?code={{ activation_code }}
{% endblocktrans %}
{% endblock %}