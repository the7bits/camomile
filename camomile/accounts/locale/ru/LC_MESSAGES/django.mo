��    0      �  C         (  ^   )  %   �  �   �  q   H  �   �  5   i  �   �  �   8  S   $  &   x  )   �     �     �     �     �     �     	  	   	  $   	     ;	     G	  	   V	     `	  &   f	  3   �	  ]   �	     
     %
     5
     <
     D
     K
     Q
     Z
     c
     y
  �   �
          %     4     H     M     T  %   \     �     �     �  �  �  �   �  G   (  T  p  �   �  �   ~  d   t  �   �  ,  �  ~   �  f   _  M   �          .     N     _     z     �     �  R   �     �       #   (  !   L  I   n  B   �  �   �     �     �     �                    %     ,  '   9     a  �   {     /     F  /   f     �     �     �  $   �  #   �  %     8   :         ,                     +                   %   .                                                   	      '         
      $   /            )   -              *           !   (      #       0   "                     &                 
Thank you for your interest to our product. 

We'll send you the invite as soon as possible.
 
Your activation data are incorrect.
 
Your are registered successfully. Check your mail inbox for new message from us. 
You need activate your account by following the link at that message.
 
Your have the invite to Camomile. Follow next link for register:

%(domain)s/accounts/register/?invite=%(code)s
 
Your have the invite to Camomile. Follow next link for register:

<a href="%(domain)s/accounts/register/?invite=%(code)s">
%(domain)s/accounts/register/?invite=%(code)s</a>
 
Your invite code is incorrect or already activated.
 
Your just created new account and need to activate it by following the next link:

%(domain)s/accounts/activate/%(nickname)s/?code=%(activation_code)s
 
Your just created new account and need to activate it by following the next link:

<a href="%(domain)s/accounts/activate/%(nickname)s/?code=%(activation_code)s">
%(domain)s/accounts/activate/%(nickname)s/?code=%(activation_code)s</a>
 
Your request is successfully received. We'll send you invite as soon as possible.
 A user with that email already exists. A user with that nickname already exists. Activation code Activation error Active Admin status Code Customer Customers Customers must have an email address Date joined Date requested Date used Email Enter an email, nickname and password. Enter the same password as above, for verification. First, enter an email, nickname and password. Then, you'll be able to edit more user options. Hello Important dates Invite Invites Log in Login Nickname Password Password confirmation Permissions Raw passwords are not stored, so there is no way to see this user's password, but you can change the password using <a href="password/">this form</a>. Register Request invite Send invite to mail Sent Submit Success The two password fields didn't match. Unknown invite You are already authorized. Your request is added to queue Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-02-16 09:09+0200
PO-Revision-Date: 2013-02-16 09:19+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 
Большое спасибо за интерес к нашему продукту. 

Мы пришлем Вам инвайт как можно скорее.
 
Ваши регистрационные данные не верны.
 
Вы успешно зарегистрированы. Проверьте входящие в вашем почтовом ящике на наличие нового сообщения от нас. 
Вам необходимо произвести активацию аккаунта перейдя по ссылке в сообщении.
 
Вы получили инвайт на Camomile. Используйте следующую ссылку для регистрации:

%(domain)s/accounts/register/?invite=%(code)s
 
Вы получили инвайт на Camomile. Используйте следующую ссылку для регистрации:

<a href="%(domain)s/accounts/register/?invite=%(code)s">
%(domain)s/accounts/register/?invite=%(code)s</a>
 
Код вашего инвайте не правильный или уже использован.
 
Вы создали новый аккаунт и должны активировать его перейдя по следующей ссылке:

%(domain)s/accounts/activate/%(nickname)s/?code=%(activation_code)s
 
Вы создали новый аккаунт и должны активировать его перейдя по следующей ссылке:

<a href="%(domain)s/accounts/activate/%(nickname)s/?code=%(activation_code)s">
%(domain)s/accounts/activate/%(nickname)s/?code=%(activation_code)s</a>
 
Ваш запрос успешно получен. Мы пришлем Вам инвайт как только сможем.
 Пользователь с такой электронной почтой уже существует Пользователь с таким ником уже существует Код активации Ошибка активации Активный Администратор Код Клиент Клиенты Пользователь должен иметь электронную почту Дата регистрации Дата запроса Дата использования Электронная почта Введите электронную почту, ник и пароль. Введите пароль еще раз, для проверки Сначала введите электронную почту, ник и пароль. Далее вы сможете отредактировать другие пользовательские настройки. Привет Важные даты Инвайт Инвайты Вход Вход Ник Пароль Подтверждение пароля Права доступа Пароль не сохраняется в чистом виде, поэтому нет возможности его узнать, но вы можете его изменить Регистрация Запросить инвайт Отправить инвайт на почту Отправлено Отправить Готово Пароли не совпадают Неизвестный инвайт Вы уже авторизованы. Ваш запрос поставлен в очередь 