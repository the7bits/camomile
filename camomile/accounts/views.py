# coding: utf-8
from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.views.generic.edit import CreateView, FormView
from django.utils.decorators import method_decorator
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login

from accounts.forms import InviteCreationForm, CustomerCreationForm
from accounts.models import Customer, Invite, ALREADY_ACTIVATED
from accounts.utils import get_random_string
from mail.utils import send_mail

class InviteCreate(CreateView):
    form_class = InviteCreationForm
    template_name = 'accounts/invite_new.html'
    success_url = 'success'

class CustomerRegister(FormView):
    form_class = CustomerCreationForm
    template_name = 'accounts/customer_register.html'
    success_url = 'success'

    def form_valid(self, form):
        nickname = form.cleaned_data['nickname']
        activation_code = get_random_string()
        customer = Customer.objects.create_user(
                email=form.cleaned_data['email'],
                nickname=nickname,
                password=form.cleaned_data['password1']
                )
        customer.is_active = False
        customer.activation_code = activation_code
        customer.save()

        invite = Invite.objects.get(is_active=True, code=self.request.GET['invite'])
        invite.is_active = False
        invite.date_used = timezone.now()
        invite.registered_customer = customer
        invite.save(update_fields=['is_active', 'date_used', 'registered_customer'])

        send_mail(to_emails=[customer.email],
                from_email=settings.DEFAULT_FROM_EMAIL,
                subject=_('Hello'),
                html_template='accounts/register_mail_message.html',
                text_template='accounts/register_mail_message.txt',
                data={
                        'domain': getattr(settings, 'SITE_DOMAIN', ''), 
                        'nickname': nickname,
                        'activation_code': activation_code,
                    })
        return super(CustomerRegister, self).form_valid(form)

def customer_activate(request, nickname):
    try:
        customer = Customer.objects.get(nickname=nickname, is_active=False)
        code = request.GET.get('code', '')
        if customer.activation_code == code:
            customer.activation_code = ALREADY_ACTIVATED
            customer.is_active = True
            customer.save(update_fields=['activation_code', 'is_active'])
            return redirect('/accounts/login/')
        else:
            return render(request, 'accounts/activation_error.html')    
    except Customer.DoesNotExist:
        return render(request, 'accounts/activation_error.html')


