# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin

from accounts.models import Customer, Invite
from accounts.forms import CustomerChangeForm, CustomerCreationForm, InviteCreationForm

class CustomerAdmin(UserAdmin):
    add_form_template = 'accounts/admin/auth/user/add_form.html'
    fieldsets = (
        (None, {'fields': ('email', 'nickname', 'password', 'activation_code')}),
        (_('Permissions'), {'fields': ('is_active', 'is_admin', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'nickname', 'password1', 'password2')}
        ),
    )

    list_display = ('nickname', 'email', 'is_admin', 'is_active')
    list_filter = ('is_admin', 'is_superuser', 'is_active', 'groups')
    search_fields = ('nickname', 'email')
    ordering = ('nickname',)
    date_hierarchy = 'date_joined'
    filter_horizontal = ('groups', 'user_permissions',)

    form = CustomerChangeForm
    add_form = CustomerCreationForm


class InviteAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_requested'
    list_display = ('email', 'code', 'is_sent', 'is_active', 'date_requested')
    list_filter = ('is_sent', 'is_active')
    actions = ['send_invite']
    add_form = InviteCreationForm

    def get_form(self, request, obj=None, **kwargs):
        """
        Use special form during invite creation
        """
        defaults = {}
        if obj is None:
            defaults.update({
                'form': self.add_form,
            })
        defaults.update(kwargs)
        return super(InviteAdmin, self).get_form(request, obj, **defaults)

    def send_invite(self, request, queryset):
        for q in queryset:
            q.send_to_mail()
    send_invite.short_description = _('Send invite to mail')


admin.site.register(Customer, CustomerAdmin)
admin.site.register(Invite, InviteAdmin)

