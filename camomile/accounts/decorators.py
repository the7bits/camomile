# coding: utf-8
from django.http import HttpResponseRedirect
from accounts.models import Invite

def invite_required(function=None, failed_url='/accounts/no-invite/'):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if 'invite' in request.GET:
                try:                    
                    invite_req = Invite.objects.get(
                            is_active=True, 
                            code=request.GET.get('invite', ''))
                    return function(request, *args, **kwargs)
                except Invite.DoesNotExist:
                    return HttpResponseRedirect(failed_url)
            return HttpResponseRedirect(failed_url)
        return _view

    if function:
        return _dec(function)
    else:
        return _dec
