# coding: utf-8
import string
import random

def get_random_string(size=50):
    allowed = string.ascii_letters
    randomstring = ''.join([allowed[random.randint(0, len(allowed) - 1)] \
            for x in xrange(size)])
    return randomstring
