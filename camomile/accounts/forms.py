# coding: utf-8
from __future__ import unicode_literals

from django import forms
from django.utils.translation import ugettext, ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from accounts.models import Customer, Invite

class CustomerChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=_('Password'),
        help_text=_('Raw passwords are not stored, so there is no way to see '
                    'this user\'s password, but you can change the password '
                    'using <a href="password/">this form</a>.'))

    class Meta:
        model = Customer
    
    def __init__(self, *args, **kwargs):
        super(CustomerChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial['password']

class CustomerCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given email, nickname and
    password.
    """
    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'duplicate_nickname': _("A user with that nickname already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = Customer
        fields = ('email', 'nickname',)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            Customer.objects.get(email=email)
        except Customer.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'])


    def clean_nickname(self):
        nickname = self.cleaned_data['nickname']
        try:
            Customer.objects.get(nickname=nickname)
        except Customer.DoesNotExist:
            return nickname
        raise forms.ValidationError(self.error_messages['duplicate_nickname'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        customer = super(CustomerCreationForm, self).save(commit=False)
        customer.set_password(self.cleaned_data["password1"])
        if commit:
            customer.save()
        return customer


class InviteCreationForm(forms.ModelForm):
    class Meta:
        model = Invite
        fields = ('email',)
