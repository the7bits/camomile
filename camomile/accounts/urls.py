# coding: utf-8
from django.conf.urls.defaults import patterns, include, url
from django.contrib.auth.views import login, logout
from django.views.generic import TemplateView

from accounts.views import InviteCreate, CustomerRegister, customer_activate
from accounts.decorators import invite_required

urlpatterns = patterns('',
    url(r'^login/$', login, {'template_name': 'accounts/login.html'}),
    url(r'^logout/$', logout, {'next_page': '/'}),
    url(r'^request-invite/$', InviteCreate.as_view(), name='request_invite'),
    url(r'^request-invite/success/$', TemplateView.as_view(template_name="accounts/invite_success.html")),
    url(r'^register/$', invite_required(CustomerRegister.as_view()), name='customer_register'),
    url(r'^register/success/$', TemplateView.as_view(template_name="accounts/customer_register_success.html")),
    url(r'^no-invite/$', TemplateView.as_view(template_name="accounts/no_invite.html")),
    url(r'^activate/(?P<nickname>\w+)/$', customer_activate),
)