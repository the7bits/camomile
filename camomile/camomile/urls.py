# coding: utf-8
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = patterns(
    '',
    (r'^django-rq/', include('django_rq.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^app/', include('projects.urls')),
    (r'^dashboard/$', login_required(TemplateView.as_view(template_name="dashboard.html"))),
    (r'^$', TemplateView.as_view(template_name="index.html")),
)

if getattr(settings, 'DEBUG', False):
    urlpatterns += patterns(
        'django.contrib.staticfiles.views',
        url(r'^static/(?P<path>.*)$', 'serve'),
    )
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': getattr(settings, 'MEDIA_ROOT', ''),
            }),
    )
