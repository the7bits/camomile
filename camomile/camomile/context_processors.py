# coding: utf-8
from django.conf import settings

def main(request):
    """Context processor for Camomile
    """
    return {
        "DOMAIN": getattr(settings, 'DEFAULT_DOMAIN', ''),
    }
